 
-- DESCRIPTION --

Exporting physical object source files to Thingiverse.com
(provided you have suitable content on your Drupal site)  

Thingiverse API-to-be documentation:
http://www.thingiverse.com/api/


-- INSTALLATION --

Install as usual

.stl, .obj, .cdr and .eps files are looked for in the node's filefields 
(might be handling exports differently in the future), list of them is provided
in the node view. Files are exported by clicking the filename on the list and 
node title and description fields are submitted along with it to Thingiverse. 


-- NOTES --

* Thingiverse does not currently allow more than one file to be sent. 
Being able to send more than one file related to one object is an important
requirement however, 